# SerialToKeyboard

_Performs a keyboard stroke for a previously mapped serial input (byte) value._

## Setup

### Config
Edit the serial port settings in the `config.yaml` file as desired. You can create a `config.yaml` by copying [`config.yaml.example`](src/main/python/config.yaml.example) and renaming it to `config.yaml`. 

#### Config file scheme

##### Serial
Configure serial settings, like port and baud rate.

##### Logging
Configure additional logging settings

##### Keystroke mappings
Under `strokes.mappings`, keystroke mappings can be added. This mapping is done by specifying the serial input byte as the key (in hex notation). The value is the to be pressed keys, as a list. 
```yaml
strokes:
  mappings:
    '0x00':   # Pressing 'a'
      - a
```

For more complex mappings, multidimensional listing can be used, like this:
```yaml
strokes:
  mappings:
    '0x00':   # Pressing, selecting, and copying a character 'a'
      - a
      - [shift, left]     # Specify a hotkey as this (preferred notation for readability)
      - - ctrl            # Or as this
        - c
```

<b>Note:</b> you can also edit and run the [config_yaml_mappings_creator](tools/config_yaml_mappings_creator.py) from the [tools](tools) directory if you are unfamiliar with YAML. Just edit the dictionary, run the script and copy and paste the output in your config.yaml.

On each element of the list, a [keyPress](https://pyautogui.readthedocs.io/en/latest/keyboard.html#the-press-keydown-and-keyup-functions) action will be performed. If a element is a (sub)list, a [hotKey](https://pyautogui.readthedocs.io/en/latest/keyboard.html#the-hotkey-function) action will be performed with  each element of the sublist in the given order. 


### Run
Just run [`main.py`](src/main/python/main.py) to start the application. A tray icon will be created. Right click on this icon and you can _start_ listening to the serial device. You can also start the serial input _identifier_. It will directly display the received serial value. You can use this value for creating mappings.

The serial port will be closed with either the _Stop_ or _Exit_ action.


## Dependencies

* [pySerial](https://pyserial.readthedocs.io/en/latest/index.html) - for communication with the serial device
* [PyAutoGUI](https://pyautogui.readthedocs.io/en/latest/index.html) - for keyboard/mouse control
* [PyYAML](https://pyyaml.org/wiki/PyYAMLDocumentation) - for loading configuration
* [PyQt5](https://pypi.org/project/PyQt5/) - for rendering GUI's