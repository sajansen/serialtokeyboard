import sys
import yaml


mappings = {
  "0x00": ['b'],
  "0x01": ['a', ['shift', 'left'], ['ctrl', 'b'], 'c', ['ctrl', 'd']],
}


def main():
  yaml.dump({"strokes": {"mappings": mappings}}, sys.stdout)


if __name__ == "__main__":
  main()
