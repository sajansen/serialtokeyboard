import unittest

from src.main.python import utils
from src.main.python.config import Config


class TestConfig(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    utils.setup_logging()

  def test_hex_string_to_byte(self):
    self.assertEqual(Config.hex_string_to_key("0x00"), '00')
    self.assertEqual(Config.hex_string_to_key("0x01"), '01')
    self.assertEqual(Config.hex_string_to_key("0xfa"), 'fa')

  def test_load_config_file_withValidContent(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    Config.load_config_file('resources/testConfigYaml_valid.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM1")
    self.assertEqual(Config.COM_baud_rate, 9600)

  def test_load_config_file_withInValidContent(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    Config.load_config_file('resources/testConfigYaml_invalid.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM1")
    self.assertEqual(Config.COM_baud_rate, 9600)
    self.assertFalse(hasattr(Config, 'invalidstuff'))

  def test_load_config_file_withInValidSyntax(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    with self.assertRaises(Exception):
      Config.load_config_file('resources/testConfigYaml_invalidSyntax.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM0")
    self.assertEqual(Config.COM_baud_rate, 9600)

  def test_setup_config_withOutFile(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    Config.setup_config(config_file_path='somwhere/over/the/rainbow.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM0")
    self.assertEqual(Config.COM_baud_rate, 9600)

  def test_load_config_file_withAdvancedMappings(self):
    # when
    Config.load_config_file('resources/testConfigYaml_advancedMappings.yaml')

    # then
    self.assertEqual(Config.strokeMappings.get('00'), ['b'])
    self.assertEqual(Config.strokeMappings.get('01'), ['a', ['shift', 'left'], ['ctrl', 'b'], 'c', ['ctrl', 'd']])

  def test_load_config_file_raisesExceptionWhenStrokeMappingIsNotList(self):
    Config.strokeMappings = {}

    # when
    with self.assertRaises(SyntaxError):
      Config.load_config_file('resources/testConfigYaml_nolistMappings.yaml')

    # then
    self.assertTrue('01' not in Config.strokeMappings.keys())
