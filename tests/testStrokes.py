import logging
import unittest
from unittest.mock import Mock, call

from src.main.python import strokes, utils

from src.main.python.config import Config


class TestMain(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    utils.setup_logging()

  def setUp(self):
    strokes.pyautogui = Mock()

  def test_setStrokeForValueAddsNewKeyToEmptyList(self):
    Config.strokeMappings = {}

    strokes.set_stroke_for_value('00', ['a'])

    self.assertEqual(len(Config.strokeMappings), 1)

  def test_setStrokeForValueAddsNewKey(self):
    Config.strokeMappings = {
      '00': ['a']
    }

    strokes.set_stroke_for_value('01', ['a'])

    self.assertEqual(len(Config.strokeMappings), 2)

  def test_setStrokeForValuOverwritesExistingKey(self):
    Config.strokeMappings = {
      '00': ['a']
    }

    strokes.set_stroke_for_value('00', ['b'])

    self.assertEqual(len(Config.strokeMappings), 1)
    self.assertEqual(Config.strokeMappings.get('00'), ['b'])

  def test_executeStrokeForValueThrowsExceptionOnInvalidKey(self):
    Config.strokeMappings = {
      '00': ['a']
    }

    with self.assertRaises(IndexError):
      strokes.execute_stroke_for_value('01')

  def test_executeStrokeForValueExecutesStrokesForKey(self):
    Config.strokeMappings = {
      '00': ['a'],
      '01': ['b']
    }

    strokes.execute_stroke_for_value('00')

    strokes.pyautogui.press.assert_called_once_with('a')
    strokes.pyautogui.hotkey.assert_not_called()

  def test_executeStrokeForValueExecutesStrokesForSpecialCharKey(self):
    Config.strokeMappings = {
      '00': ['a'],
      b'\x09': ['b']  # Also known as '\t'
    }

    strokes.execute_stroke_for_value(b'\t')

    strokes.pyautogui.press.assert_called_once_with('b')
    strokes.pyautogui.hotkey.assert_not_called()

  def test_executeStrokeForValueExecutesHotkeyStrokesForKey(self):
    Config.strokeMappings = {
      '00': [['ctrl', 'a']]
    }

    strokes.execute_stroke_for_value('00')

    strokes.pyautogui.press.assert_not_called()
    strokes.pyautogui.hotkey.assert_called_once_with('ctrl', 'a')

  def test_executeStrokeForValueExecutesHotkeyAndNormalStrokesForKey(self):
    Config.strokeMappings = {
      '00': ['a', ['ctrl', 'a'], 'b', ['ctrl', 'c']]
    }

    strokes.execute_stroke_for_value('00')

    strokes.pyautogui.press.assert_has_calls([call('a'), call('b')])
    strokes.pyautogui.hotkey.assert_has_calls([call('ctrl', 'a'), call('ctrl', 'c')])
