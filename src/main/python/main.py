import logging
import os
import sys

from src.main.python import utils
from src.main.python.config import Config
from src.main.python.gui.trayicon import SystemTraiIcon

logger = logging.getLogger(__name__)

def _append_run_path():
  if getattr(sys, 'frozen', False):
    pathlist = []

    # If the application is run as a bundle, the pyInstaller bootloader
    # extends the sys module by a flag frozen=True and sets the app
    # path into variable _MEIPASS'.
    pathlist.append(sys._MEIPASS)

    # the application exe path
    _main_app_path = os.path.dirname(sys.executable)
    pathlist.append(_main_app_path)

    # append to system path enviroment
    os.environ["PATH"] += os.pathsep + os.pathsep.join(pathlist)

  logging.debug("current PATH: %s", os.environ['PATH'])


def main(argv):
  Config.setup_config()
  utils.setup_logging()
  _append_run_path()

  from PyQt5.QtWidgets import QApplication
  app = QApplication(sys.argv)
  tray = SystemTraiIcon(parent=app)
  tray.show()

  sys.exit(app.exec_())


if __name__ == "__main__":
  main(sys.argv[1:])
