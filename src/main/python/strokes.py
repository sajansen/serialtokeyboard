import logging
import pyautogui
import serial
from pymsgbox import alert

from src.main.python import utils
from src.main.python.config import Config

logger = logging.getLogger(__name__)


def set_stroke_for_value(value, strokes):
  logger.debug("Mapping strokes %s to value 0x%s", strokes, value)
  Config.strokeMappings[value] = strokes


def execute_stroke_for_value(value):
  strokes = Config.strokeMappings.get(value)

  if not strokes:
    raise IndexError("No strokes defined for value: 0x{}".format(value))

  logger.info("Executing stroke for 0x%s", value)
  for stroke in strokes:
    if isinstance(stroke, list):
      pyautogui.hotkey(*stroke)
    else:
      pyautogui.press(stroke)


def interact_with_serial_port():
  Config.setup_config()
  utils.setup_logging()

  with serial.Serial(Config.COM_port, Config.COM_baud_rate) as ser:
    logger.info("Connected to serial port: %s", ser.name)
    ser.reset_input_buffer()

    while True:
      value = ser.read().hex()
      logger.debug("Received value: 0x%s", value)
      try:
        execute_stroke_for_value(value)
      except:
        pass


def identify_serial_values():
  Config.setup_config()
  utils.setup_logging()

  with serial.Serial(Config.COM_port, Config.COM_baud_rate) as ser:
    logger.info("Connected to serial port: %s", ser.name)
    ser.reset_input_buffer()

    while True:
      value = ser.read().hex()
      logger.info("Identified value: 0x%s", value)
      alert(text="0x{}".format(value), title='Identified value')
