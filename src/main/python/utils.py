import logging

from src.main.python.config import Config


def setup_logging():
  logging.basicConfig(level=Config.logging_level,
                      format=Config.logging_format)
