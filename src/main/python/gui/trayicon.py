import logging
from multiprocessing import Process

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QSystemTrayIcon, QMenu

from src.main.python import strokes
from src.main.python.config import Config

logger = logging.getLogger(__name__)


class SystemTraiIcon(QSystemTrayIcon):

  def __init__(self, parent=None):
    super().__init__(QIcon(Config.GUI_trayIconDefault), parent)

    self.isSerialListening = False
    self.serialListeningProcess = None

    self.isSerialIdentifying = False
    self.identifySerialInputProcess = None

    self.setToolTip("Not running")

    menu = QMenu()
    self.setContextMenu(menu)

    self.serial_listening_start_action = menu.addAction("Start")
    self.serial_listening_start_action.setIcon(QIcon(Config.GUI_startIcon))
    self.serial_listening_start_action.triggered.connect(self.start_serial_listening)

    self.serial_listening_stop_action = menu.addAction("Stop")
    self.serial_listening_stop_action.setEnabled(False)
    self.serial_listening_stop_action.setIcon(QIcon(Config.GUI_stopIcon))
    self.serial_listening_stop_action.triggered.connect(self.stop_all_processes)

    self.serial_listening_identify_action = menu.addAction("Identify")
    self.serial_listening_identify_action.setIcon(QIcon(Config.GUI_questionIcon))
    self.serial_listening_identify_action.triggered.connect(self.start_identify_serial_input)

    self.exit_action = menu.addAction("Exit")
    self.exit_action.setIcon(QIcon(Config.GUI_exitIcon))
    self.exit_action.triggered.connect(self.exit_app)

    logger.info("Application started")

  def start_serial_listening(self):
    self.serialListeningProcess = Process(target=strokes.interact_with_serial_port)
    self.serialListeningProcess.start()
    logger.info("Started listening")

    self.isSerialListening = True
    self.serial_listening_start_action.setEnabled(False)
    self.serial_listening_stop_action.setEnabled(True)
    self.serial_listening_identify_action.setEnabled(False)
    self.setToolTip("Running")
    self.setIcon(QIcon(Config.GUI_trayIconRunning))

  def stop_serial_listening(self):
    self.serialListeningProcess.terminate()
    self.isSerialListening = False
    logger.info("Stopped listening")

  def start_identify_serial_input(self):
    self.identifySerialInputProcess = Process(target=strokes.identify_serial_values)
    self.identifySerialInputProcess.start()
    logger.info("Started identification process")

    self.isSerialIdentifying = True
    self.serial_listening_start_action.setEnabled(False)
    self.serial_listening_stop_action.setEnabled(True)
    self.serial_listening_identify_action.setEnabled(False)
    self.setToolTip("Waiting for input for identicifation")
    self.setIcon(QIcon(Config.GUI_trayIconRunning))

  def stop_identify_serial_input_process(self):
    self.identifySerialInputProcess.terminate()
    self.isSerialIdentifying = False
    logger.info("Stopped identification process")

  def stop_all_processes(self):
    if self.isSerialListening:
      self.stop_serial_listening()

    if self.isSerialIdentifying:
      self.stop_identify_serial_input_process()

    self.serial_listening_start_action.setEnabled(True)
    self.serial_listening_stop_action.setEnabled(False)
    self.serial_listening_identify_action.setEnabled(True)
    self.setToolTip("Not running")
    self.setIcon(QIcon(Config.GUI_trayIconDefault))

  def exit_app(self):
    self.stop_all_processes()

    self.parent().exit()
