# This class contains all the settings for running the project. Edits can be made here.
import logging
import os
import yaml

logger = logging.getLogger(__name__)

class Config:
  ##
  # Serial communication
  ##

  COM_port = 'COM14'
  COM_baud_rate = 9600

  ##
  # Logging
  ##

  # Logging level as one of Python logging levels (DEBUG, INFO, WARNING, ERROR, ...)
  logging_level = "DEBUG"
  logging_format = "[%(asctime)s | %(name)s | %(levelname)-8s] %(message)s"

  ##
  # Default stroke mappings
  ##

  # Each mapping consists of a byte read from the serial port as the dictonary key
  # and the to-be-executed strokes as the value. This value must be a list of
  # key presses and/or key strokes (sublists).
  strokeMappings = {}

  ##
  # GUI
  ##

  GUI_trayIconDefault = "../icons/base/64.png"
  GUI_trayIconRunning = "../icons/base/running-64.png"
  GUI_startIcon = "resources/icons/start-32.png"
  GUI_stopIcon = "resources/icons/stop-32.png"
  GUI_exitIcon = "resources/icons/exit-32.png"
  GUI_questionIcon = "resources/icons/question-32.png"

  @staticmethod
  def setup_config(config_file_path='config.yaml'):
    if os.path.isfile(config_file_path):
      Config.load_config_file(config_file_path)

  @staticmethod
  def load_config_file(file_path):
    with open(file_path, 'r') as stream:
      yaml_config = yaml.safe_load(stream)

    if 'serial' in yaml_config.keys():
      Config.COM_port = yaml_config['serial'].get('port', Config.COM_port)
      Config.COM_baud_rate = yaml_config['serial'].get('baud_rate', Config.COM_baud_rate)

    if 'logging' in yaml_config.keys():
      Config.logging_level = yaml_config['logging'].get('level', Config.logging_level)
      Config.logging_format = yaml_config['logging'].get('format', Config.logging_format)

    if 'strokes' in yaml_config.keys():
      if 'mappings' in yaml_config['strokes'].keys() and isinstance(yaml_config['strokes']['mappings'], dict):
        for mapping_key, mapping_value in yaml_config['strokes']['mappings'].items():
          if not isinstance(mapping_value, list):
            raise SyntaxError("Stroke mappings must contain a list as values for each mapping. {} is not a list!"
                              .format(mapping_value))
          Config.strokeMappings[Config.hex_string_to_key(mapping_key)] = mapping_value

  @staticmethod
  def hex_string_to_key(string):
    return string[2:]    # Strip '0x'
